const {iso, isoShort, utc, longDate} = require('./filters/date');
const {limit} = require('./filters/utils');

const slugify = require('./filters/slugify')

module.exports = function (config, options) {
	config.addFilter('dateISO', iso);
	config.addFilter('dateISOShort', isoShort);
	config.addFilter('dateUTC', utc);
	config.addFilter('dateLong', longDate);

	config.addFilter('limit', limit);
	config.addFilter('slugify', slugify);
};
