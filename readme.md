A collection of utilities I found myself using on every project. Open to MR/PR/Issues/suggestions

- [Installation](#installation)
- [Usage](#usage)
- [Filters](#filters)
- [Slugify](#slugify)
- [Utils](#utils)
	- [Limit](#limit)
	- [Date](#date)
		- [ISO](#iso)
		- [UTC](#utc)
		- [Long Date](#long-date)
		- [Day](#day)
		- [Day with Ordinal](#day-with-ordinal)
		- [Month](#month)
		- [Month Name](#month-name)
		- [Year](#year)
- [To Do](#to-do)

## Installation

```
npm i --save @mikestreety/11ty-utils
```

In your `.eleventy.js`, you then add it as a plugin

```js
const utils = require("@mikestreety/11ty-utils");

module.exports = (eleventyConfig) => {
	eleventyConfig.addPlugin(utils);
};
```

## Usage

Below are the utils included within the package. Along with using them in 11ty, you can also include the Javascript file and `require` the module you need.

For example, if you want to use the `dayOridinal` filter as a function in a data file, you can do:

```js
const {dayOrdinal} = require('@mikestreety/11ty-utils/filters/date');

//...

dayOrdinal(post.date);
```

## Filters

These are the filters included within the Utils

## Slugify

Turn a string into a URL friendly string (or slug). This is particularly useful, especially when building pagination or creating a collection [from a JSON feed](https://www.mikestreety.co.uk/blog/creating-an-11ty-collection-from-json-api/).

**Example output:** `This is a string` becomes `this-is-a-string`

- **Filter:** `{{ post.data.title | slugify }}`
- **Function:** `slugify(date)`

For use with a Javascript file:

```js
const slugify = require('@mikestreety/11ty-utils/filters/slugify');
```

## Utils

### Limit

This is useful if you want to limit the number of posts on your homepage, for example.

**Example use:**

```
{% set postslist = collections.blog | limit(6) %}
{%- for entry in postslist -%}
	//
{% endfor %}
```

### Date

If you require the functions in a Javascript file, you can require them, only including the functions you need. For example:

```js
const {iso, utc} = require('@mikestreety/11ty-utils/filters/date');
```

#### ISO

Returns an ISO formatted date string , used for things like schema markup

**Example output:** `2011-10-05T14:48:00.000Z`

- **Filter:** `{{ date | dateISO }}`
- **Function:** `iso(date)`

[Docs link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString)

#### ISO Short

Returns an ISO formatted date string without the time - useful for sitemaps

**Example output:** `2011-10-05`

- **Filter:** `{{ date | dateISOShort }}`
- **Function:** `isoShort(date)`

[Docs link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString)

#### UTC

Returns a rfc7231 style UTC date string. For use with RSS

**Example output:** `Tue, 06 Apr 2021 00:00:00 GMT`

- **Filter:** `{{ date | dateUTC }}`
- **Function:** `utc(date)`

[Docs link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toUTCString)

#### Long Date

A long date formatted with the ordinal and long month name

**Example output:**  `22nd March 2021`

- **Filter:** `{{ date | dateLong }}`
- **Function:** `longDate(date)`

#### Day

The day in number format

**Example output:**  `22` or `6`

- **Function:** `day(date)`

#### Day with Ordinal

The day in number format with the ordinal (`nth`, `rd`) appended

**Example output:**  `22nd` or `6th`

- **Function:** `dayOrdinal(date)`

#### Month

The month in a numerical format

**Example output:**  `1` or `11`

- **Function:** `month(date)`

#### Month Name

The full name of the month

**Example output:**  `March` or `December`

- **Function:** `monthName(date)`

#### Year

The full year

**Example output:**  `2021` or `1995`

- **Function:** `year(date)`

## To Do

- Add `truncate`
  - add `truncateWords` which does a number of words
  - `truncateChars` will do characters (meta desc as default)
- Add `dateShort` in a DD/MM/YY format (although, America?)

